# Web Scene Core - jvm Module
Server side library for building web UIs for Kotlin JVM projects.


## Installation

To install the library do the following:

1. Clone the [webscene-core](https://gitlab.com/webscene/webscene-core) Git repository from GitHub
2. Import the cloned repository into IntelliJ
3. Create all JAR files (source, documentation, library) by running the **publish** Gradle task
4. Copy the [build/repository/org](build/repository/org) directory to this directory in your project: **libs**
5. Assuming Gradle is used in your project edit your **build.gradle.kts** file, and insert the following:

```kotlin
import java.net.URI

// ...

repositories {
    mavenCentral()
    maven {
        url = URI("libs").toURL()
    }
}
```

6. Add this line in your **build.gradle.kts** file to add the library as a dependency:

```kotlin
compile("org.webscene:webscene-core-jvm:version")
```


## Basic Usage

Use the **org.webscene.server.WebScene** object to create HTML elements. Below is an example:

```kotlin
import org.webscene.server.html.HtmlCreator as html

fun main(args: Array<String>) {
    html.parentElement("div") {
        parentElement("p") {
            element("b") { +"Hello World! :)" }
        }
    }
}
```

Once the HTML element is created you call the **createText** function off the object to generate the HTML that the web server will send as part of a HTTP response. Here is what the HTML will look like after calling the **createText** function (based on the example above):

```html
<div>
    <p>
        <b>Hello World! :)</b>
    </p>
</div>
```
