package org.webscene.core.template

import org.webscene.core.html.HtmlTag

@Suppress("unused")
/** Template for a section of a web page. */
class SectionTemplate : HtmlTemplate {
    override var content: HtmlTag? = null
}
