package org.webscene.core.template

import org.webscene.core.html.HtmlTag

/** Base for an HTML template. */
interface HtmlTemplate {
    /** The root HTML element to use for the template. */
    var content: HtmlTag?
}
