@file:Suppress("PackageDirectoryMismatch")

package org.webscene.core.test.dom

import org.w3c.dom.*
import org.webscene.core.dom.DomEditType
import org.webscene.core.dom.DomEditor
import org.webscene.core.html.HtmlSection
import org.webscene.core.html.element.toDomElement
import org.webscene.core.test.contains
import org.webscene.core.test.totalChildElements
import org.webscene.core.html.HtmlInputCreator as htmlInput
import kotlin.browser.document
import kotlin.dom.clear
import kotlin.test.*
import org.webscene.core.html.HtmlCreator as html

class DomEditorTest {
    @BeforeTest
    fun setup() {
        document.head!!.clear()
        document.body!!.clear()
    }

    @Test
    fun testEditBodySectionWithPrepend() {
        val heading = html.heading {
            id = "heading"
            +"Hello World! :)"
        }
        document.body!!.appendChild(document.createElement("p"))
        DomEditor.editSection(domElement = heading.toDomElement())
        assertEquals(expected = 2, actual = document.body!!.childElementCount,
            message = "The body element must contain two elements")
        val element = document.body!!.firstElementChild
        assertTrue(actual = element != null && element.id == heading.id,
            message = "Heading hasn't been prepended to the body element")
    }

    @Test
    fun testEditNavigationSectionWithPrepend() {
        val nav = html.parentElement("nav") {
            children += htmlInput.button { +"Button 2" }
        }.toDomElement()
        val btn = htmlInput.button {
            id = "btn1"
            +"Button 1"
        }
        document.body!!.appendChild(nav)
        DomEditor.editSection(htmlSection = HtmlSection.NAVIGATION, domElement = btn.toDomElement())
        assertEquals(expected = 2, actual = nav.childElementCount,
            message = "The nav element must contain two elements")
        assertTrue(actual = nav.firstElementChild!!.id == btn.id,
            message = "The button hasn't been prepended to the nav element")
    }

    @Test
    fun testEditNavigationSectionWithAppend() {
        val nav = html.parentElement("nav") { children += htmlInput.button { +"Button 1" } }.toDomElement()
        val btn = htmlInput.button {
            id = "btn2"
            +"Button 2"
        }
        document.body!!.appendChild(nav)
        DomEditor.editSection(domElement = btn.toDomElement(), editType = DomEditType.APPEND,
            htmlSection = HtmlSection.NAVIGATION)
        assertEquals(expected = 2, actual = nav.childElementCount,
            message = "The nav element must contain two elements")
        assertTrue(actual = nav.children[1]!!.id == btn.id,
            message = "The button hasn't been appended to the nav element")
    }

    @Test
    fun testEditNavigationSectionWithRemove() {
        val btn = htmlInput.button { +"Button 3" }.toDomElement()
        val nav = html.parentElement("nav") {
            children += htmlInput.button { +"Button 1" }
            children += htmlInput.button { +"Button 2" }
        }.toDomElement()
        nav.appendChild(btn)
        document.body!!.appendChild(nav)
        DomEditor.editSection(htmlSection = HtmlSection.NAVIGATION, domElement = btn,
            editType = DomEditType.REMOVE)
        assertEquals(expected = 2, actual = nav.childElementCount,
            message = "The nav element must contain only one element")
        assertTrue(actual = btn !in nav.children,
            message = "The button hasn't been removed from the nav element")
    }

    @Test
    fun testEditHeaderSectionWithPrepend() {
        val header = html.header { children += html.paragraph { +"Something" } }.toDomElement()
        val paragraph = html.paragraph {
            id = "item1"
            +"Acme Ltd"
        }
        document.body!!.appendChild(header)
        DomEditor.editSection(htmlSection = HtmlSection.HEADER, domElement = paragraph.toDomElement())
        assertEquals(expected = 2, actual = header.childElementCount,
            message = "The header element must contain two elements")
        assertTrue(actual = header.firstElementChild!!.id == paragraph.id,
            message = "The paragraph hasn't been prepended to the header element")
    }

    @Test
    fun testEditHeaderSectionWithAppend() {
        val header = html.header { children += html.paragraph { +"Something" } }.toDomElement()
        val paragraph = html.paragraph {
            id = "item2"
            +"Acme Ltd"
        }
        document.body!!.appendChild(header)
        DomEditor.editSection(domElement = paragraph.toDomElement(), editType = DomEditType.APPEND,
            htmlSection = HtmlSection.HEADER)
        assertEquals(expected = 2, actual = header.childElementCount,
            message = "The header element must contain two elements")
        assertTrue(actual = header.children[1]!!.id == paragraph.id,
            message = "The paragraph hasn't been appended to the header element")
    }

    @Test
    fun testEditHeaderSectionWithRemove() {
        val paragraph = html.paragraph { +"Item 3" }.toDomElement()
        val header = html.header {
            children += html.paragraph { +"Item 1" }
            children += html.paragraph { +"Item 2" }
        }.toDomElement()
        header.appendChild(paragraph)
        document.body!!.appendChild(header)
        DomEditor.editSection(htmlSection = HtmlSection.HEADER, domElement = paragraph,
            editType = DomEditType.REMOVE)
        assertEquals(expected = 2, actual = header.childElementCount,
            message = "The header element must contain only one element")
        assertTrue(actual = paragraph !in header.children,
            message = "The paragraph hasn't been removed from the header element")
    }

    @Test
    fun testEditFooterSectionWithPrepend() {
        val footer = html.footer { children += html.paragraph { +"Something" } }.toDomElement()
        val paragraph = html.paragraph {
            id = "item1"
            +"Acme Ltd"
        }
        document.body!!.appendChild(footer)
        DomEditor.editSection(htmlSection = HtmlSection.FOOTER, domElement = paragraph.toDomElement())
        assertEquals(expected = 2, actual = footer.childElementCount,
            message = "The footer element must contain two elements")
        assertTrue(actual = footer.firstElementChild!!.id == paragraph.id,
            message = "The paragraph hasn't been prepended to the footer element")
    }

    @Test
    fun testEditFooterSectionWithAppend() {
        val footer = html.footer { children += html.paragraph { +"Something" } }.toDomElement()
        val paragraph = html.paragraph {
            id = "item2"
            +"Acme Ltd"
        }
        document.body!!.appendChild(footer)
        DomEditor.editSection(domElement = paragraph.toDomElement(), editType = DomEditType.APPEND,
            htmlSection = HtmlSection.FOOTER)
        assertEquals(expected = 2, actual = footer.childElementCount,
            message = "The footer element must contain two elements")
        assertTrue(actual = footer.children[1]!!.id == paragraph.id,
            message = "The paragraph hasn't been appended to the footer element")
    }

    @Test
    fun testEditFooterSectionWithRemove() {
        val paragraph = html.paragraph { +"Item 3" }.toDomElement()
        val footer = html.footer {
            children += html.paragraph { +"Item 1" }
            children += html.paragraph { +"Item 2" }
        }.toDomElement()
        footer.appendChild(paragraph)
        document.body!!.appendChild(footer)
        DomEditor.editSection(htmlSection = HtmlSection.FOOTER, domElement = paragraph,
            editType = DomEditType.REMOVE)
        assertEquals(expected = 2, actual = footer.childElementCount,
            message = "The footer element must contain only one element")
        assertTrue(actual = paragraph !in footer.children,
            message = "The paragraph hasn't been removed from the footer element")
    }

    @Test
    fun testEditHeadSectionWithPrepend() {
        val meta = html.element("meta") {
            id = "misc"
        }.toDomElement()
        val head = document.head!!
        head.appendChild(document.createElement("meta"))
        DomEditor.editSection(domElement = meta, htmlSection = HtmlSection.HEAD)
        assertEquals(expected = 2, actual = head.childElementCount,
            message = "The head element must contain two elements")
        assertTrue(actual = head.firstElementChild == meta,
            message = "The meta element hasn't been prepended to the head element")
    }

    @Test
    fun testEditHeadSectionWithAppend() {
        val meta = html.element("meta") {
            id = "misc"
        }.toDomElement()
        val head = document.head!!
        head.appendChild(document.createElement("meta"))
        DomEditor.editSection(domElement = meta, editType = DomEditType.APPEND, htmlSection = HtmlSection.HEAD)
        assertEquals(expected = 2, actual = head.childElementCount,
            message = "The head element must contain two elements")
        assertTrue(actual = head.children[1] == meta,
            message = "The meta element hasn't been appended to the head element")
    }

    @Test
    fun testEditHeadSectionWithRemove() {
        val meta = html.element("meta") {
            id = "misc"
        }.toDomElement()
        val head = document.head!!
        head.appendChild(meta)
        head.appendChild(document.createElement("meta"))
        DomEditor.editSection(htmlSection = HtmlSection.HEAD, domElement = meta,
            editType = DomEditType.REMOVE)
        assertEquals(expected = 1, actual = head.childElementCount,
            message = "The head element must contain only one element")
        assertTrue(actual = meta !in head.children,
            message = "The meta element hasn't been removed from the head element")
    }

    @Test
    fun testEditBodySectionWithAppend() {
        assertFailsWith(IllegalArgumentException::class) {
            DomEditor.editSection(editType = DomEditType.APPEND, domElement = document.createElement("p"))
        }
    }

    @Test
    fun testEditBodySectionWithRemove() {
        val header = html.header { +"Header" }.toDomElement()
        document.body!!.appendChild(header)
        assertFailsWith(IllegalArgumentException::class) {
            DomEditor.editSection(editType = DomEditType.REMOVE, domElement = header)
        }
    }

    @Test
    fun testRemoveExistingElementById() {
        val body = document.body!!
        val keyTxt = document.createElement("b").apply { id = "key-txt" }
        body.appendChild(document.createElement("h1").apply { id = "heading" })
        body.appendChild(keyTxt)
        body.appendChild(document.createElement("p").apply { id = "txt-block" })
        assertTrue(actual = DomEditor.removeElementById(keyTxt.id),
            message = "The removeElementById function should return true")
        assertFalse(actual = keyTxt in body.children, message = "Element still exists")
        assertEquals(expected = 2, actual = totalChildElements(body),
            message = "The body element doesn't contain only two elements")
    }

    @Test
    fun testRemoveNonExistingElementById() {
        assertFalse(actual = DomEditor.removeElementById("heading"),
            message = "The removeElementById function should return false")
    }

    @Test
    fun testReplaceElement() {
        val heading = html.heading {
            id = "heading"
            +"Hello World! :)"
        }
        val body = document.body!!
        body.appendChild(heading.toDomElement())
        body.appendChild(document.createElement("p"))
        DomEditor.replaceElement {
            heading.txtContent = "Title"
            heading
        }
        assertEquals(expected = "Title", actual = body.children[0]?.textContent ?: "",
            message = "The heading element hasn't been replaced")
    }
}
