@file:Suppress("PackageDirectoryMismatch")

package org.webscene.core.test.dom

import org.w3c.dom.Element
import org.w3c.dom.HTMLTextAreaElement
import org.w3c.dom.get
import org.webscene.core.dom.DomQuery
import kotlin.browser.document
import kotlin.dom.addClass
import kotlin.dom.clear
import kotlin.dom.hasClass
import kotlin.test.*

class DomQueryTest {
    @BeforeTest
    fun setup() {
        document.head!!.clear()
        document.body!!.clear()
    }

    @Test
    fun testAllElementsByClassNames() {
        val className = "txt-block"
        document.body!!.appendChild(document.createElement("h1").apply {
            addClass("header")
        })
        document.body!!.appendChild(document.createElement("p").apply {
            addClass(className)
        })
        document.body!!.appendChild(document.createElement("p").apply {
            addClass(className)
        })

        val foundElements = DomQuery.allElementsByClassNames(className)
        assertEquals(expected = 2, actual = foundElements.size, message = "Number of found elements don't match")
        assertTrue(actual = foundElements.all { it.hasClass(className) },
            message = "Found some elements that don't match the class name ($className)")
    }

    @Test
    fun testElementWithIdExists() {
        document.body!!.appendChild(document.createElement("h1").apply {
            id = "main-header"
        })
        document.body!!.appendChild(document.createElement("p").apply {
            id = "txt-block"
        })
        assertTrue(DomQuery.elementWithIdExists("main-header"),
            message = "The elementWithIdExists function should return true")
        assertFalse(DomQuery.elementWithIdExists("form"),
            message = "The elementWithIdExists function should return false")
    }

    @Test
    fun testElementById() {
        document.body!!.appendChild(document.createElement("h1").apply {
            id = "main-header"
        })
        document.body!!.appendChild(document.createElement("p").apply {
            id = "txt-block"
        })
        assertEquals(expected = "txt-block", actual = DomQuery.elementById<Element>("txt-block").id,
            message = "No element found matching the id")
        assertFailsWith(exceptionClass = NoSuchElementException::class,
            message = "A NoSuchElementException isn't thrown") {
            DomQuery.elementById<HTMLTextAreaElement>("editor")
        }
    }

    @Test
    fun testTagNameExists() {
        document.body!!.appendChild(document.createElement("h1"))
        document.body!!.appendChild(document.createElement("p"))
        assertTrue(actual = DomQuery.tagNameExists("p"),
            message = "The tagNameExists function should return true with p tag")
        assertFalse(actual = DomQuery.tagNameExists("b"),
            message = "The tagNameExists function should return false with b tag")
    }

    @Test
    fun testAllElementsByTagName() {
        val body = document.body!!
        body.appendChild(document.createElement("h1"))
        body.appendChild(document.createElement("p"))
        body.appendChild(document.createElement("b"))
        body.appendChild(document.createElement("p"))
        body.appendChild(document.createElement("p"))
        val elements = DomQuery.allElementsByTagName("p")
        assertEquals(expected = 3, actual = elements.length, message = "Total elements found don't match")
        var allElementsMatch = true
        for (pos in 0..(elements.length - 1)) {
            val tmp = elements[pos]
            if (tmp != null && tmp.tagName.toLowerCase() != "p") {
                allElementsMatch = false
                break
            }
        }
        assertTrue(actual = allElementsMatch, message = "All elements must have the same tag name")
    }
}
