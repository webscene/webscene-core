@file:Suppress("PackageDirectoryMismatch")

package org.webscene.core.test.html

import org.w3c.dom.get
import org.webscene.core.html.WebPage
import kotlin.browser.document
import kotlin.browser.window
import kotlin.dom.clear
import kotlin.test.*

class WebPageTest {
    @BeforeTest
    fun setup() {
        window.onload = null
        document.head!!.clear()
        document.body!!.clear()
    }

    @Test
    fun testChangePageId() {
        WebPage.changePageId("home-page")
        val pageId = document.head?.children?.get(0)?.getAttribute("pageId") ?: ""
        assertEquals(expected = "home-page", actual = pageId, message = "The page ID hasn't been changed")
    }

    @Test
    fun testFetchExistingPageId() {
        document.head!!.appendChild(document.createElement("meta").apply {
            setAttribute("pageId", "home-page")
        })
        document.body!!.appendChild(document.createElement("meta").apply {
            setAttribute("resolution", "800x600")
        })
        document.body!!.appendChild(document.createElement("p").apply {
            setAttribute("pageId", "test")
        })
        assertEquals(expected = "home-page", actual = WebPage.fetchPageId(), message = "Incorrect page ID")
    }

    @Test
    fun testFetchNonExistingPageId() {
        assertEquals(expected = "", actual = WebPage.fetchPageId(), message = "Page ID should be empty")
    }

    @Test
    fun testAddScript() {
        WebPage.addScript("dummy.js")
        WebPage.addScript("temp.js")
        val scriptElements = document.getElementsByTagName("script")
        var total = 0
        (0..(scriptElements.length - 1)).forEach { pos ->
            val tmp = scriptElements[pos]
            if (tmp != null && tmp.getAttribute("type") == "application/javascript") total++
        }
        assertEquals(expected = 2, actual = total, message = "Total script elements doesn't match")
    }

    @Test
    fun testChangeOnLoad() {
        WebPage.changeOnLoad {}
        assertNotNull(actual = window.onload, message = "No event handler has been set")
    }

    @Test
    fun testFetchOnLoad() {
        window.onload = {}
        assertNotNull(actual = WebPage.fetchOnLoad(), message = "A event handler has to be returned")
    }
}