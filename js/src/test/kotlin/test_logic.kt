@file:Suppress("PackageDirectoryMismatch")

package org.webscene.core.test

import org.w3c.dom.Element
import org.w3c.dom.HTMLCollection
import org.w3c.dom.Node
import org.w3c.dom.get

internal fun totalChildElements(element: Element): Int {
    var result = 0
    (0..(element.children.length - 1)).forEach { n ->
        if (element.children[n]?.nodeType == Node.ELEMENT_NODE) result++
    }
    return result
}

internal operator fun HTMLCollection.contains(element: Element): Boolean {
    var result = false
    for (pos in 0..(length - 1)) {
        if (this[pos] == element) {
            result = true
            break
        }
    }
    return result
}
