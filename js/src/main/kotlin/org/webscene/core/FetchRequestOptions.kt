package org.webscene.core

import org.w3c.fetch.*

/**
 * Contains advanced request options to use with the [fetchData] function.
 * @param referrerPolicy The referrer policy to use.
 * @param cacheBehavior The cache behavior to use.
 * @param redirectBehavior The redirect behavior to use.
 * @param credentialsBehavior The credentials behavior to use.
 * @param mode The request mode to use.
 * @param integrity The hash to use.
 */
data class FetchRequestOptions(
    val referrerPolicy: String = "no-referrer",
    val cacheBehavior: RequestCache? = RequestCache.DEFAULT,
    val redirectBehavior: RequestRedirect? = RequestRedirect.FOLLOW,
    val credentialsBehavior: RequestCredentials = RequestCredentials.SAME_ORIGIN,
    val mode: RequestMode = RequestMode.SAME_ORIGIN,
    val integrity: String = ""
)
