@file:Suppress("PackageDirectoryMismatch", "PackageName")

package org.webscene.core.html.element.input.form_element_ext

import org.w3c.dom.Element
import org.webscene.core.html.element.ParentHtmlElement
import org.webscene.core.html.element.input.FormElement
import org.webscene.core.html.element.toDomElement

fun FormElement.toDomElement(): Element {
    updateAttributes()
    return (this as ParentHtmlElement).toDomElement()
}
