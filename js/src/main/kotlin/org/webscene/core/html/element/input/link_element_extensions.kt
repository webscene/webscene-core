@file:Suppress("PackageName", "PackageDirectoryMismatch")

package org.webscene.core.html.element.input.link_element_ext

import org.w3c.dom.Element
import org.webscene.core.html.element.ParentHtmlElement
import org.webscene.core.html.element.input.LinkElement
import org.webscene.core.html.element.toDomElement

fun LinkElement.toDomElement(): Element {
    updateAttributes()
    return (this as ParentHtmlElement).toDomElement()
}
