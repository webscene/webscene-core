@file:Suppress("unused")

package org.webscene.core.html.element

import org.w3c.dom.Element
import org.webscene.core.html.HtmlTag
import org.webscene.core.html.element.input.FormElement
import org.webscene.core.html.element.input.LinkElement
import org.webscene.core.html.element.input.link_element_ext.toDomElement
import org.webscene.core.html.element.input.form_element_ext.toDomElement
import org.webscene.core.html.element.image_element_ext.toDomElement
import org.webscene.core.html.element.ordered_list_element_ext.toDomElement
import org.webscene.core.html.element.unordered_list_element_ext.toDomElement
import kotlin.browser.document
import kotlin.dom.addClass
import kotlin.dom.appendText
import kotlin.dom.createElement

/** A collection of handlers for processing a child tag *before* it is converted to a [DOM element][Element]. */
val childTagHandlers: MutableList<(parentElement: Element, childTag: HtmlTag) -> Unit> = mutableListOf()

fun HtmlElement.toDomElement(): Element {
    val tmpAttributes = attributes
    val tmpId = id
    updateAttributes()

    return document.createElement(tagName) {
        addClass(*classes.toTypedArray())
        tmpAttributes.forEach { (key, value) -> setAttribute(key, value) }
        id = tmpId
        if (txtContent.isNotEmpty()) appendText(txtContent)
    }
}

fun ParentHtmlElement.toDomElement(): Element {
    val tmpId = id
    val tmpAttributes = attributes
    val tmpChildren = children

    return document.createElement(tagName) {
        updateAttributes()
        addClass(*classes.toTypedArray())
        tmpAttributes.forEach { (key, value) -> setAttribute(key, value) }
        id = tmpId
        if (txtContent.isNotEmpty()) appendText(txtContent)

        tmpChildren.forEach { tag ->
            val originalCount = childElementCount
            for (handler in childTagHandlers) {
                handler(this, tag)
                if (originalCount != childElementCount) break
            }
            if (originalCount == childElementCount) processHtmlInputElement(this, tag)
            if (originalCount == childElementCount) processHtmlElement(this, tag)
        }
    }
}

private fun processHtmlElement(parentElement: Element, childTag: HtmlTag) {
    when (childTag) {
        is FormElement -> parentElement.appendChild(childTag.toDomElement())
        is ImageElement -> parentElement.appendChild(childTag.toDomElement())
        is ParentHtmlElement -> parentElement.appendChild(childTag.toDomElement())
        is HtmlElement -> parentElement.appendChild(childTag.toDomElement())
    }
}

private fun processHtmlInputElement(parentElement: Element, childTag: HtmlTag) {
    when (childTag) {
        is LinkElement -> parentElement.appendChild(childTag.toDomElement())
        is OrderedListElement -> parentElement.appendChild(childTag.toDomElement())
        is UnorderedListElement -> parentElement.appendChild(childTag.toDomElement())
    }
}
