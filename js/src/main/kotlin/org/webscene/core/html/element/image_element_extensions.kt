@file:Suppress("PackageDirectoryMismatch", "PackageName")

package org.webscene.core.html.element.image_element_ext

import org.w3c.dom.Element
import org.webscene.core.html.element.ImageElement
import org.webscene.core.html.element.ParentHtmlElement
import org.webscene.core.html.element.toDomElement

fun ImageElement.toDomElement(): Element {
    updateAttributes()
    return (this as ParentHtmlElement).toDomElement()
}
