package org.webscene.core.html

import org.w3c.dom.Element
import org.w3c.dom.events.Event
import org.w3c.dom.get
import org.webscene.core.dom.DomEditType
import org.webscene.core.dom.DomEditor
import org.webscene.core.dom.findAllElementsByTagName
import org.webscene.core.html.element.toDomElement
import kotlin.browser.document
import kotlin.browser.window
import org.webscene.core.html.HtmlCreator as html

object WebPage {
    fun changeOnLoad(handler: (Event) -> dynamic) {
        window.onload = handler
    }

    fun fetchOnLoad(): ((Event) -> dynamic)? = window.onload

    /** Changes the unique identifier for the web page (pageId). Will create one if it doesn't exist. */
    fun changePageId(pageId: String) {
        if (fetchPageId().isEmpty()) {
            DomEditor.editSection(htmlSection = HtmlSection.HEAD, editType = DomEditType.PREPEND,
                domElement = html.parentElement("meta") { attributes["pageId"] = pageId }.toDomElement())
        }
    }

    /**
     * Obtains the unique identifier for the web page (pageId).
     * @return The page ID if it is found otherwise a empty [String].
     */
    fun fetchPageId(): String {
        val metaElements = document.findAllElementsByTagName("meta")
        var element: Element? = null
        for (pos in 0..(metaElements.length - 1)) {
            val tmp = metaElements[pos]
            if (tmp != null && tmp.hasAttribute("pageId")) {
                element = tmp
                break
            }
        }
        return if (element != null) element.getAttribute("pageId") ?: "" else ""
    }

    /**
     * Adds a new script element to the end of the body in the web page.
     * @param src Path to the script (can be a absolute or relative path).
     * @param type MIME type to use.
     */
    fun addScript(src: String, type: String = "application/javascript") {
        document.body?.appendChild(html.script(src, type).toDomElement())
    }
}
