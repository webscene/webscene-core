@file:Suppress("PackageDirectoryMismatch", "PackageName")

package org.webscene.core.html.element.ordered_list_element_ext

import org.w3c.dom.Element
import org.webscene.core.html.element.OrderedListElement
import org.webscene.core.html.element.ParentHtmlElement
import org.webscene.core.html.element.toDomElement

fun OrderedListElement.toDomElement(): Element {
    updateAttributes()
    return (this as ParentHtmlElement).toDomElement()
}
