package org.webscene.core.dom

import org.w3c.dom.Element
import org.webscene.core.html.HtmlSection
import org.webscene.core.html.HtmlTag
import kotlin.browser.document

@Suppress("unused")
/** Contains common functionality for editing the DOM. */
object DomEditor {
    /**
     * Edits a [section][htmlSection] of the DOM. Do note that this function doesn't support replacing an existing DOM
     * element in a [section][htmlSection], instead use [replaceElement] function.
     * @param htmlSection The HTML section to apply the edit.
     * @param domElement The DOM element to use in the edit.
     * @param editType Type of DOM edit to apply.
     */
    fun editSection(
        htmlSection: HtmlSection = HtmlSection.BODY,
        domElement: Element,
        editType: DomEditType = DomEditType.PREPEND
    ) {
        when (htmlSection) {
            HtmlSection.BODY -> editBody(domElement, editType)
            HtmlSection.HEAD -> editHead(domElement, editType)
            HtmlSection.FOOTER -> editFooter(domElement, editType)
            HtmlSection.HEADER -> editHeader(domElement, editType)
            HtmlSection.NAVIGATION -> editNavigation(domElement, editType)
        }
    }

    private fun editNavigation(domElement: Element, editType: DomEditType) {
        when (editType) {
            DomEditType.PREPEND -> document.prependElement(domElement, HtmlSection.NAVIGATION)
            DomEditType.APPEND -> document.appendElement(domElement, HtmlSection.NAVIGATION)
            DomEditType.REMOVE -> document.removeElement(domElement, HtmlSection.NAVIGATION)
        }
    }

    private fun editHeader(domElement: Element, editType: DomEditType) {
        when (editType) {
            DomEditType.PREPEND -> document.prependElement(domElement, HtmlSection.HEADER)
            DomEditType.APPEND -> document.appendElement(domElement, HtmlSection.HEADER)
            DomEditType.REMOVE -> document.removeElement(domElement, HtmlSection.HEADER)
        }
    }

    private fun editHead(domElement: Element, editType: DomEditType) {
        when (editType) {
            DomEditType.APPEND -> document.appendElement(domElement, HtmlSection.HEAD)
            DomEditType.PREPEND -> document.prependElement(domElement, HtmlSection.HEAD)
            DomEditType.REMOVE -> document.removeElement(domElement, HtmlSection.HEAD)
        }
    }

    private fun editBody(domElement: Element, editType: DomEditType) {
        // Only DomEditType.PREPEND is supported for performance reasons:
        // https://stackoverflow.com/questions/4396849/does-the-script-tag-position-in-html-affects-performance-of-the-webpage
        if (editType == DomEditType.PREPEND) document.prependElement(domElement, HtmlSection.BODY)
        else throw IllegalArgumentException("Only DomEditType.PREPEND is supported for the editType parameter.")
    }

    private fun editFooter(domElement: Element, editType: DomEditType) {
        when (editType) {
            DomEditType.APPEND -> document.appendElement(domElement, HtmlSection.FOOTER)
            DomEditType.PREPEND -> document.prependElement(domElement, HtmlSection.FOOTER)
            DomEditType.REMOVE -> document.removeElement(domElement, HtmlSection.FOOTER)
        }
    }

    /**
     * Replaces an existing [DOM element][Element] with a new one. The ID for the element to be replaced **MUST** exist
     * in the DOM.
     * @param block Function for replacing the old element with the [new element][HtmlTag] (must have its ID set),
     * which must be returned in the last line.
     */
    fun replaceElement(block: () -> HtmlTag) = document.replaceElement(block)

    /**
     * Removes an existing DOM element by its ID.
     * @param id Unique identifier of the DOM element.
     * @return A value of true if the element has been removed, false otherwise.
     */
    fun removeElementById(id: String): Boolean = document.removeElementById(id)
}
