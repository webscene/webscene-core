@file:Suppress("unused")

package org.webscene.core.dom

import org.w3c.dom.*
import org.webscene.core.html.HtmlSection
import org.webscene.core.html.HtmlTag
import org.webscene.core.html.element.HtmlElement
import org.webscene.core.html.element.ParentHtmlElement
import org.webscene.core.html.element.toDomElement
import kotlin.browser.document

// Extensions for org.w3c.dom.Document.

internal fun Document.replaceElement(block: () -> HtmlTag) {
    val htmlTag = block()
    val domElement = document.findElementById(htmlTag.id)

    if (htmlTag is ParentHtmlElement) domElement?.replaceWith(htmlTag.toDomElement())
    else if (htmlTag is HtmlElement) domElement?.replaceWith(htmlTag.toDomElement())
}

/**
 * Removes an existing DOM element. The element **MUST** have its ID attribute set.
 * @param domElement The [element][Element] to remove from the DOM.
 */
internal fun Document.removeElement(domElement: Element) = document.removeElementById(domElement.id)

/**
 * Removes an existing DOM element from a [section][htmlSection].
 * @param domElement The [element][Element] to remove from the DOM.
 * @param htmlSection The HTML section to use.
 */
internal fun Document.removeElement(domElement: Element, htmlSection: HtmlSection) {
    when (htmlSection) {
        HtmlSection.NAVIGATION -> {
            val nav = document.findAllElementsByTagName("nav")[0]
            nav?.removeChild(domElement)
        }
        HtmlSection.HEAD -> {
            document.head?.removeChild(domElement)
        }
        HtmlSection.HEADER -> {
            val header = document.findAllElementsByTagName("header")[0]
            header?.removeChild(domElement)
        }
        HtmlSection.FOOTER -> {
            val footer = document.findAllElementsByTagName("footer")[0]
            footer?.removeChild(domElement)
        }
        else -> {
            document.body?.removeChild(domElement)
        }
    }
}

internal fun Document.removeElementById(id: String): Boolean {
    val domElement = document.findElementById(id)
    domElement?.remove()
    return domElement != null
}

/**
 * Prepends a [DOM element][Element] to a [HTML section][htmlSection].
 * @param domElement The DOM element to prepend.
 * @param htmlSection The HTML section to use.
 */
internal fun Document.prependElement(domElement: Element, htmlSection: HtmlSection) {
    when (htmlSection) {
        HtmlSection.NAVIGATION -> {
            val nav = document.findAllElementsByTagName("nav")[0]
            nav?.prepend(domElement)
        }
        HtmlSection.HEAD -> {
            document.head?.prepend(domElement)
        }
        HtmlSection.HEADER -> {
            val header = document.findAllElementsByTagName("header")[0]
            header?.prepend(domElement)
        }
        HtmlSection.FOOTER -> {
            val footer = document.findAllElementsByTagName("footer")[0]
            footer?.prepend(domElement)
        }
        else -> {
            document.body?.prepend(domElement)
        }
    }
}

/**
 * Appends a [DOM element][Element] to a [HTML section][htmlSection].
 * @param domElement The DOM element to append.
 * @param htmlSection The HTML section to use.
 */
internal fun Document.appendElement(domElement: Element, htmlSection: HtmlSection) {
    when (htmlSection) {
        HtmlSection.NAVIGATION -> {
            val nav = document.findAllElementsByTagName("nav")[0]
            nav?.appendChild(domElement)
        }
        HtmlSection.HEAD -> {
            document.head?.append(domElement)
        }
        HtmlSection.HEADER -> {
            val header = document.findAllElementsByTagName("header")[0]
            header?.appendChild(domElement)
        }
        HtmlSection.FOOTER -> {
            val footer = document.findAllElementsByTagName("footer")[0]
            footer?.appendChild(domElement)
        }
        else -> {
            document.body?.append(domElement)
        }
    }
}

internal fun Document.findAllElementsByTagName(tagName: String): HTMLCollection =
    document.getElementsByTagName(tagName)

internal fun Document.findElementById(id: String): Element? = document.getElementById(id)

internal fun Document.findAllElementsByClassNames(vararg classNames: String) = document.getElementsByClassName(
    classNames.joinToString(separator = " ")).asList()
