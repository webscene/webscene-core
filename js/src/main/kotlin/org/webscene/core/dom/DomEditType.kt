package org.webscene.core.dom

enum class DomEditType {
    APPEND, PREPEND, REMOVE
}
