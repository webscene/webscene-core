package org.webscene.core.dom

import org.w3c.dom.Element
import org.w3c.dom.HTMLCollection
import kotlin.browser.document

@Suppress("unused")
/** Contains common functionality for doing a DOM query. */
object DomQuery {
    /**
     * Retrieves all DOM elements that match [tagName].
     * @param tagName Name of the tag.
     * @return A list of all DOM elements that match [tagName].
     */
    fun allElementsByTagName(tagName: String): HTMLCollection = document.findAllElementsByTagName(tagName)

    @Suppress("UNCHECKED_CAST")
        /**
         * Retrieves the [DOM element][T] that matches [id].
         * @param id Unique identifier for the [DOM element][Element].
         * @return A [DOM element][T] if there is a match.
         * @throws NoSuchElementException If there is no element matching the generic type or there is no element
         * matching the [id].
         */
    fun <T : Element> elementById(id: String): T = try {
        document.findElementById(id) as T
    } catch (ex: ClassCastException) {
        throw NoSuchElementException("Couldn't find element $id, or the type doesn't match.")
    }

    /**
     * Retrieves all DOM [elements][Element] that match one or more [class names][classNames].
     * @param classNames One or more names of classes.
     * @return A list of all [DOM elements][Element] that match [classNames].
     */
    fun allElementsByClassNames(vararg classNames: String): List<Element> =
        document.findAllElementsByClassNames(*classNames)

    /** Checks if a [element][Element] with the id exists in the DOM. */
    fun elementWithIdExists(id: String): Boolean = document.findElementById(id) != null

    /** Checks if the tag name exists in the DOM. */
    fun tagNameExists(tagName: String): Boolean = document.findAllElementsByTagName(tagName).length > 0

    @Suppress("UNCHECKED_CAST")
        /**
         * Retrieves the [DOM element][T] that matches the [selector].
         * @param selector Custom query string.
         * @return A [DOM element][T] if there is a match.
         * @throws NoSuchElementException If there is no element matching the generic type or there is no element
         * matching the [selector].
         */
    fun <T : Element> elementBySelector(selector: String) = try {
        document.querySelector(selector) as T
    } catch (ex: ClassCastException) {
        throw NoSuchElementException("Couldn't find element by selector ($selector), or the type doesn't match.")
    }
}
