@file:Suppress("unused")

package org.webscene.core.dom

import org.w3c.dom.Element
import org.w3c.dom.asList
import org.w3c.dom.get
import org.webscene.core.html.element.HtmlElement
import org.webscene.core.html.element.ParentHtmlElement
import org.webscene.core.html.element.toDomElement

// Extensions for org.w3c.dom.Element.

/**
 * Removes all HTML attributes from the [DOM element][org.w3c.dom.Element].
 */
fun Element?.clearAttributes() {
    this?.attributes?.asList()?.map { it.name }?.forEach { attributes.removeNamedItem(it) }
}

/**
 * Appends a [parent HTML element][ParentHtmlElement] to this [DOM element][org.w3c.dom.Element].
 * @param tagName Name of the tag.
 * @param block Initialisation block for setting up the [parent HTML element][ParentHtmlElement].
 */
fun Element?.appendParentHtmlElement(tagName: String, block: ParentHtmlElement.() -> Unit) {
    val parentHtmlElement = ParentHtmlElement()

    parentHtmlElement.tagName = tagName
    parentHtmlElement.block()
    this?.append(parentHtmlElement.toDomElement())
}

/**
 * Prepends a [parent HTML element][ParentHtmlElement] to this [DOM element][org.w3c.dom.Element].
 * @param tagName Name of the tag.
 * @param block Initialisation block for setting up the [parent HTML element][ParentHtmlElement].
 */
fun Element?.prependParentHtmlElement(tagName: String, block: ParentHtmlElement.() -> Unit) {
    val parentHtmlElement = ParentHtmlElement()

    parentHtmlElement.tagName = tagName
    parentHtmlElement.block()
    this?.prepend(parentHtmlElement.toDomElement())
}

/**
 * Appends a [HTML element][HtmlElement] to this [DOM element][org.w3c.dom.Element].
 * @param tagName Name of the tag.
 * @param block Initialisation block for setting up the [HTML element][HtmlElement].
 */
fun Element?.appendHtmlElement(tagName: String, block: HtmlElement.() -> Unit) {
    val htmlElement = HtmlElement()

    htmlElement.tagName = tagName
    htmlElement.block()
    this?.append(htmlElement.toDomElement())
}

/**
 * Prepends a [HTML element][HtmlElement] to this [DOM element][org.w3c.dom.Element].
 * @param tagName Name of the tag.
 * @param block Initialisation block for setting up the [HTML element][HtmlElement].
 */
fun Element?.prependHtmlElement(tagName: String, block: HtmlElement.() -> Unit) {
    val htmlElement = HtmlElement()

    htmlElement.tagName = tagName
    htmlElement.block()
    this?.prepend(htmlElement.toDomElement())
}

/**
 * Changes the focus to the DOM element.
 */
fun Element?.focus() {
    if (this?.localName == "input") this.asDynamic().focus()
}

/** Returns the value of the DOM element if it is a text box, otherwise a empty String. */
val Element?.textBoxValue: String
    get() = if (this?.localName == "input" && this.attributes["type"]?.value == "text") {
        asDynamic().value.toString()
    } else {
        ""
    }

/** Changes the value of the DOM element if it is a text box. */
fun Element?.textBoxValue(txt: String) {
    if (this?.localName == "input" && attributes["type"]?.value == "text") {
        asDynamic().value = txt
    }
}

/** Returns the value of the DOM element if it is a text area, otherwise a empty String. */
val Element?.textAreaValue: String
    get() = if (this?.localName == "textarea") asDynamic().value.toString() else ""

/** Changes the value of the DOM element if it is a text area. */
fun Element?.textAreaValue(txt: String) {
    if (this?.localName == "textarea") asDynamic().value = txt
}

fun Element?.disabled(newValue: Boolean) {
    if (this != null && newValue) this.setAttribute("disabled", "$newValue")
    else if (this != null && !newValue) this.removeAttribute("disabled")
}

val Element?.disabled: Boolean
    get() = this != null && this.hasAttribute("disabled")

fun Element?.hidden(newValue: Boolean) {
    if (this != null && newValue) this.setAttribute("hidden", "$newValue")
    else if (this != null && !newValue) this.removeAttribute("hidden")
}

val Element?.hidden: Boolean
    get() = this != null && this.hasAttribute("hidden")

/** Changes the style for a DOM element. If newValue is empty then the style attribute is removed. */
fun Element?.style(newValue: String) {
    if (newValue.isEmpty()) this?.removeAttribute("style")
    else this?.setAttribute("style", newValue)
}

/** Returns the style for a DOM element, otherwise a empty String. */
val Element?.style: String
    get() = if (this != null && this.hasAttribute("style")) {
        this.getAttribute("style") ?: ""
    } else {
        ""
    }
