@file:Suppress("unused")

package org.webscene.core.dom

import org.w3c.dom.events.Event

// Extensions for org.w3c.dom.events.Event.

val Event.keyCode: String
    get() = this.asDynamic().keyCode.toString()
