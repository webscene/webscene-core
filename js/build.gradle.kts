import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile
import com.moowork.gradle.node.NodeExtension
import com.moowork.gradle.node.npm.NpmTask
import com.moowork.gradle.node.task.NodeTask
import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.dsl.KotlinProjectExtension

// TODO: Uncomment the line below.
//import org.jetbrains.dokkaPlugin.gradle.DokkaTask

val moduleName = "$rootProjectName-${project.name}"

plugins {
    `maven-publish`
    id(PluginId.node) version Version.nodePlugin
}

repositories {
    jcenter()
    mavenCentral()
}

// TODO: Remove the block below.
//publishing {
//    publications {
//        create("docs", MavenPublication::class.java) {
//            from(components["java"])
//            artifact(createDokkaJar)
//        }
//        create("sources", MavenPublication::class.java) {
//            from(components["java"])
//            artifact(createSourceJar)
//        }
//    }
//
//    repositories {
//        maven { url = uri("$buildDir/repository") }
//    }
//}

buildscript {
    repositories {
        mavenCentral()
        jcenter()
    }

    dependencies {
        classpath(kotlin(module = "gradle-plugin", version = Version.kotlin))
        // TODO: Uncomment the line below.
//        classpath(Dependency.dokkaPlugin)
        classpath(Dependency.nodePlugin)
    }
}

apply {
    // TODO: Uncomment the line below.
//    plugin(PluginId.dokka)
    plugin(PluginId.kotlinPlatformJs)
    plugin(PluginId.kotlin2Js)
    plugin(PluginId.node)
    from("${rootProject.rootDir.absolutePath}/publishing.gradle")
}

configure<NodeExtension> {
    version = Version.node
    download = true
    nodeModulesDir = file("$projectDir/node_modules")
}

dependencies {
    "expectedBy"(project(":common"))
    "compile"(kotlin(module = "stdlib-js", version = Version.kotlin))
    "testCompile"(Dependency.kotlinTestJs)
}

val testDir = "${buildDir.absolutePath}/test"
val libDir = "${buildDir.absolutePath}/node_modules"
val compileKotlin2Js by tasks.getting(Kotlin2JsCompile::class) {
    val destDir = "${projectDir.absolutePath}/web"
    kotlinOptions {
        outputFile = "$destDir/js/$rootProjectName-$version.js"
        sourceMap = true
        moduleKind = "umd"
    }
    doFirst { File(destDir).deleteRecursively() }
}
val compileTestKotlin2Js by tasks.getting(Kotlin2JsCompile::class) {
    kotlinOptions {
        sourceMap = true
        moduleKind = "umd"
        outputFile = "$testDir/$rootProjectName-${version}_test.js"
    }
    doLast { File("$testDir/$rootProjectName-${version}_test").deleteRecursively() }
}

// TODO: Uncomment the two blocks below.
/*
val dokka by tasks.getting(DokkaTask::class) {
    this.moduleName = moduleName
    outputDirectory = "$buildDir/javadoc"
    sourceDirs = files("src/main/kotlin")
    doFirst { File("${projectDir.absolutePath}/build/javadoc").deleteRecursively() }
}
val createDokkaJar by tasks.creating(Jar::class) {
    dependsOn(dokka)
    classifier = "javadoc"
    from(dokka.outputDirectory)
}
*/
val copyTestDependencies by tasks.creating(Copy::class) {
    dependsOn(compileKotlin2Js)
    val parentDir = File(compileKotlin2Js.kotlinOptions.outputFile).parent
    configurations["testCompile"].forEach {
        from(zipTree(it.absolutePath).matching {
            include("*.js")
        })
    }
    from(parentDir).exclude("$rootProjectName-$version")
    into(libDir)
}

apply {
    from("${projectDir.absolutePath}/testing.gradle")
}

// TODO: Uncomment the block below and remove the jest.gradle file.
//val runJest by tasks.creating(NodeTask::class) {
//    val outputUri = (compileTestKotlin2Js as Kotlin2JsCompile).outputFile.toURI()
//    dependsOn(installJest, compileTestKotlin2Js, copyTestDependencies, copyTestFiles)
//    setScript(File("$projectDir/node_modules/jest/bin/jest.js"))
//    addArgs(projectDir.toURI().relativize(outputUri))
//}
val test by tasks.getting { dependsOn("runJest") }
val clean by tasks.getting {
    doFirst { File("$projectDir/node_modules").deleteRecursively() }
}
val createSourceJar by tasks.creating(Jar::class) {
    baseName = moduleName
    dependsOn("classes")
    classifier = "sources"
    from("src/main/kotlin")
}
val jar by tasks.getting(Jar::class) { baseName = moduleName }

task("createAllJarFiles") {
    // TODO: Uncomment the line below.
//    dependsOn("jar", createSourceJar, createDokkaJar)
    // TODO: Remove the line below.
    dependsOn("jar", createSourceJar)
    println("Creating $moduleName JAR files (library, sources and documentation)...")
    doLast { println("Finished creating JAR files.") }
}

configure<KotlinProjectExtension> {
    experimental.coroutines = Coroutines.ENABLE
}
