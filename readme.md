# Web Scene Core (webscene-core)

The core library for building and managing web UIs for Kotlin projects. This library is comprised of three Kotlin multi-platform modules:

- [common](common) (the *Common* module which is required by the other modules)
- [js](js) (the *Platform* module that is used by Kotlin JS projects)
- [jvm](jvm) (the *Platform* module that is used by Kotlin JVM projects)
