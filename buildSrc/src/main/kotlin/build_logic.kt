@file:Suppress("unused")

const val rootProjectName = "webscene-core"

object Version {
    const val kotlin = "1.2.70"
    const val dokkaPlugin = "0.9.14"
    const val nodePlugin = "1.2.0"
    const val node = "8.11.4"
    const val jest = "23.3"
    const val puppeteer = "1.7.0"
}

object Dependency {
    const val dokkaPlugin = "org.jetbrains.dokka:dokka-gradle-plugin:${Version.dokkaPlugin}"
    const val kotlinTestJs = "org.jetbrains.kotlin:kotlin-test-js:${Version.kotlin}"
    const val nodePlugin = "com.moowork.gradle:gradle-node-plugin:${Version.nodePlugin}"
}

object PluginId {
    const val dokka = "org.jetbrains.dokka"
    const val kotlinPlatformCommon = "kotlin-platform-common"
    const val kotlinPlatformJvm = "kotlin-platform-jvm"
    const val kotlinPlatformJs = "kotlin-platform-js"
    const val kotlin2Js = "kotlin2js"
    const val node = "com.moowork.node"
}
