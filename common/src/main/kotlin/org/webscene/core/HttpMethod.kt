package org.webscene.core

/** General HTTP/S methods. */
enum class HttpMethod {
    /** Read a resource. **/
    GET,
    /** Sent a request containing data. **/
    POST,
    /** Upload data. **/
    PUT,
    /** Remove a resource. **/
    DELETE,
    /** Read all heading information. **/
    HEAD,
    /** Obtain a list of all HTTP/S methods that the server supports. **/
    OPTIONS,
    /** Convert request connection into TCP/IP tunnel. **/
    CONNECT
}
