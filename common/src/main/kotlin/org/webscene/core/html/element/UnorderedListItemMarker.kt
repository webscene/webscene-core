package org.webscene.core.html.element

enum class UnorderedListItemMarker(val style: String) {
    /** Sets the marker to a bullet (default). */
    DISC("list-style-type: disc"),
    CIRCLE("list-style-type: circle"),
    SQUARE("list-style-type: square"),
    /** No list items will be marked. */
    NONE("list-style-type: none")
}
