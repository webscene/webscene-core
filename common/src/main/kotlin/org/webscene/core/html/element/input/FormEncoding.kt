package org.webscene.core.html.element.input

enum class FormEncoding(val txt: String) {
    URL("application/x-www-form-urlencoded"),
    MULTI_PART("multipart/form-data"),
    TEXT("text/plain")
}
