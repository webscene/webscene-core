package org.webscene.core.html.element

import org.webscene.core.html.HtmlTag

/** Basic HTML element which doesn't contain any children. */
open class HtmlElement : HtmlTag {
    override var hidden = false
    override var disabled = false
    override var style = ""
    override var id = ""
    /** Contains unique class names. **/
    val classes = mutableSetOf<String>()
    override val attributes = mutableMapOf<String, String>()
    override lateinit var tagName: String
    override var isClosed = false
    override var txtContent = ""
    override var tooltip = ""

    private fun createAttributes() = buildString {
        updateAttributes()
        if (attributes.isNotEmpty()) {
            append(" ")
            attributes.keys.forEach { append("$it = \"${attributes[it]}\" ") }
        }
    }

    private fun createClasses() = buildString {
        if (classes.isNotEmpty()) append(" class = \"${classes.joinToString(" ")}\"")
    }

    override fun createText(indent: Int): String = buildString {
        @Suppress("ForEachParameterNotUsed")
        (1..indent).forEach { append(" ") }
        append("<$tagName${createClasses()}${createAttributes()}")
        if (isClosed) append(" />") else append(">$txtContent</$tagName>")
    }

    /**
     * Changes the [text][txtContent] to include in the HTML element.
     */
    operator fun String.unaryPlus() {
        txtContent = this
    }
}
