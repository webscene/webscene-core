package org.webscene.core.html

/** Base for a parent HTML tag that contains [children]. */
interface ParentHtmlTag : HtmlTag {
    /** HTML tags to include in the parent HTML tag. */
    val children: MutableList<HtmlTag>
}
