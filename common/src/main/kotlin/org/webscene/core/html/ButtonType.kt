package org.webscene.core.html

enum class ButtonType {
    BUTTON, RESET, SUBMIT
}
