package org.webscene.core.html

/** Base for a HTML tag. */
interface HtmlTag {
    /** Unique identifier for the HTML tag. */
    var id: String
    /** Name of the tag. */
    var tagName: String
    /** Multiple key/value entries that may be included in the HTML tag. */
    val attributes: MutableMap<String, String>
    /** If true then there is only one tag for the element, two otherwise. */
    var isClosed: Boolean
    /** Ordinary text (doesn't include HTML tags). */
    var txtContent: String
    var tooltip: String
    var hidden: Boolean
    var disabled: Boolean
    var style: String

    /**
     * Creates a text representation of HTML.
     * @param indent Number of spaces to use for indenting HTML elements.
     * @return HTML as a [String].
     */
    fun createText(indent: Int = 2): String

    /** Keeps the [attributes] property up to date. */
    fun updateAttributes() {
        if (tooltip.isNotEmpty()) attributes["title"] = tooltip else attributes -= "title"
        if (style.isNotEmpty()) attributes["style"] = style else attributes -= "style"
        if (hidden) attributes["hidden"] = "$hidden" else attributes -= "hidden"
        if (disabled) attributes["disabled"] = "$disabled" else attributes -= "disabled"
    }
}
