package org.webscene.core.html.element

/** Represents a HTML **ol** (ordered list) element. */
class OrderedListElement : ParentHtmlElement() {
    override var tagName: String
        get() = "ol"
        set(_) {}
    var type: OrderedListType = OrderedListType.NUMBERS
    /** First number to start from in the ordered list. **/
    var start: Int = 1

    override fun updateAttributes() {
        super.updateAttributes()
        if (type != OrderedListType.NUMBERS) attributes["type"] = type.txt
        if (start > 1) attributes["start"] = "$start"
    }

    override fun createText(indent: Int): String {
        updateAttributes()
        return super.createText(indent)
    }

    /**
     * Creates a new list item HTML element in [UnorderedListElement] that can contain child HTML elements.
     * @param block Initialisation block for setting up the list item element.
     * @return A new [list item HTML element][ParentHtmlElement].
     */
    @Suppress("unused")
    fun listItem(block: ParentHtmlElement.() -> Unit): ParentHtmlElement {
        val listItemElement = ParentHtmlElement()
        children.add(listItemElement)
        listItemElement.block()
        listItemElement.tagName = "li"
        return listItemElement
    }
}
