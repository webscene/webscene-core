package org.webscene.core.html.element

enum class OrderedListType(val txt: String) {
    NUMBERS("1"),
    UPPER_CASE_LETTERS("A"),
    LOWER_CASE_LETTERS("a"),
    UPPER_CASE_ROMAN_NUMBERS("I"),
    LOWER_CASE_ROMAN_NUMBERS("i")
}
