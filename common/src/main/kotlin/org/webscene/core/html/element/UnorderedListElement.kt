package org.webscene.core.html.element

/** Represents a HTML **ul** (unordered list) element. */
class UnorderedListElement : ParentHtmlElement() {
    override var tagName: String
        get() = "ul"
        set(_) {}
    var itemMarker: UnorderedListItemMarker = UnorderedListItemMarker.SQUARE

    override fun updateAttributes() {
        super.updateAttributes()
        if (itemMarker != UnorderedListItemMarker.SQUARE) {
            if ("style" in attributes) attributes["style"] = "${attributes["style"]}; ${itemMarker.style}"
            else attributes["style"] = itemMarker.style
        }
    }

    override fun createText(indent: Int): String {
        updateAttributes()
        return super.createText(indent)
    }

    /**
     * Creates a new list item HTML element in [UnorderedListElement] that can contain child HTML elements.
     * @param block Initialisation block for setting up the list item element.
     * @return A new [list item HTML element][ParentHtmlElement].
     */
    @Suppress("unused")
    fun listItem(block: ParentHtmlElement.() -> Unit): ParentHtmlElement {
        val listItemElement = ParentHtmlElement()
        children.add(listItemElement)
        listItemElement.block()
        listItemElement.tagName = "li"
        return listItemElement
    }
}
