package org.webscene.core

import org.webscene.core.html.HtmlCreator as html
import org.webscene.core.html.element.*

// Creates HTML elements.

internal fun createParentHtmlElement(tagName: String, block: ParentHtmlElement.() -> Unit): ParentHtmlElement {
    val parentHtmlElement = ParentHtmlElement()
    parentHtmlElement.block()
    parentHtmlElement.tagName = tagName
    return parentHtmlElement
}

internal fun createHtmlElement(tagName: String, block: HtmlElement.() -> Unit): HtmlElement {
    val htmlElement = HtmlElement()
    htmlElement.block()
    htmlElement.tagName = tagName
    return htmlElement
}

internal fun createHtmlHeading(level: Int = 1, block: ParentHtmlElement.() -> Unit): ParentHtmlElement {
    val headerElement = ParentHtmlElement()
    headerElement.block()
    headerElement.tagName = if (level in 1..6) "h$level" else "h1"
    return headerElement
}

internal fun createHtmlImage(src: String, alt: String = "", block: ImageElement.() -> Unit): ImageElement {
    val imgElement = ImageElement()
    imgElement.block()
    imgElement.src = src
    imgElement.alt = alt
    return imgElement
}

internal fun createOrderedList(block: OrderedListElement.() -> Unit): OrderedListElement {
    val orderedListElement = OrderedListElement()
    orderedListElement.block()
    return orderedListElement
}

internal fun createUnorderedList(block: UnorderedListElement.() -> Unit): UnorderedListElement {
    val unorderedListElement = UnorderedListElement()
    unorderedListElement.block()
    return unorderedListElement
}

internal fun createScript(
    src: String,
    type: String = "application/javascript"
) = html.parentElement("script") {
    attributes["src"] = src
    attributes["type"] = type
}
