# Web Scene Core - common Module

The *Common* module that provides core functionality used by other modules. **Note:** this module can't be used directly in a Kotlin project. Please refer to the main [readme file](../readme.md) for more details.